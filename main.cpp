#include "SDL.h"
#include <stdlib.h>
#include <string>
#include <vector>
#include <stdio.h>
#include <stack>
# include "PerlinNoise.hpp"
#include <math.h>
#include <time.h>

using namespace std;

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 800;

int const mapsize_x = 200;
int const mapsize_y = 200;
int const tileSize = 20;

struct world_object {
	int x;
	int y;
	int type;
};

struct pathfind_matrix {
	int x;
	int y;
	int h;
	bool traversable;
	bool visited;
};
struct map {
	int tiletype;
	int objecttype;
};

struct person {
	int x;
	int y;
	int prevx;
	int prevy;
	int targetx;
	int targety;
	stack<pathfind_matrix> movelist;
public:
	int getX() {
		return x * tileSize;
	}
	int getY() {
		return y * tileSize;
	}
	int getPrevY() {
		return prevy * tileSize;
	}
	int getPrevX() {
		return prevx * tileSize;
	}
	void moveTo(int x2, int y2) {
		prevx = x;
		prevy = y;
		x = x2;
		y = y2;
	}
	void cutTree() {

	}
};

unsigned long long int calculate_h(int startx, int starty, int endx, int endy) {
	int b1 = abs(endy - starty);
	int b2 = pow(b1, 2);
	int c1 = abs(endx - startx);
	int c2 = pow(c1, 2);
	return (c2 + b2);
}

stack<pathfind_matrix> pathfind(int startx, int starty, int endx, int endy, map map[mapsize_x][mapsize_y]) {
	
	int currentx = startx;
	int currenty = starty;
	unsigned long long int currenth = 0;
	pathfind_matrix matrix[mapsize_x][mapsize_y];
	
	
	stack<pathfind_matrix> mainStack;
	for (int i = 0; i < mapsize_x; i++) {
		for (int j = 0; j < mapsize_y; j++) {
			if ((map[i][j].tiletype != 0) || (map[i][j].objecttype != 0)) {
				matrix[i][j].traversable = false;
			}
			else {
				matrix[i][j].traversable = true;
			}
			matrix[i][j].x = i;
			matrix[i][j].y = j;
			matrix[i][j].visited = false;
		}
		
	}

	mainStack.push(matrix[startx][starty]);
	matrix[startx][starty].visited = true;
	bool found = false;
	unsigned long long int new_currenth;
	
	//Loop through neighbours, adding to stack and marking as visited
	while (!found) {
		int newx = -99;
		int newy = -99;
		int new_currenth = 9999999999999;
		if ((matrix[currentx + 1][currenty].traversable == true) && (matrix[currentx + 1][currenty].visited == false) && (currentx + 1 <= mapsize_x)) {
			if (calculate_h(endx, endy, currentx + 1, currenty) < new_currenth) {
				new_currenth = calculate_h(endx, endy, currentx + 1, currenty);
				newx = currentx + 1;
				newy = currenty;
			}
		}
		if ((matrix[currentx - 1][currenty].traversable == true) && (matrix[currentx - 1][currenty].visited == false) && (currentx - 1 >= 0)) {
			if (calculate_h(endx, endy, currentx - 1, currenty) < new_currenth) {
				new_currenth = calculate_h(endx, endy, currentx - 1, currenty);
				newx = currentx - 1;
				newy = currenty;
			}
		}
		if ((matrix[currentx][currenty + 1].traversable == true) && (matrix[currentx][currenty + 1].visited == false) && (currenty + 1 <= mapsize_y)) {
			if (calculate_h(endx, endy, currentx, currenty + 1) < new_currenth) {
				new_currenth = calculate_h(endx, endy, currentx, currenty + 1);
				newx = currentx;
				newy = currenty + 1;
			}
		}
		if ((matrix[currentx][currenty - 1].traversable == true) && (matrix[currentx][currenty - 1].visited == false) && (currenty -1 >= 0)) {
			if (calculate_h(endx, endy, currentx, currenty - 1) < new_currenth) {
				new_currenth = calculate_h(endx, endy, currentx, currenty - 1);
				newx = currentx;
				newy = currenty - 1;
			}
		}
		/*
		//diagonal -- RENABLE TO ALLOW DIAGONAL MOVEMENT
		if ((matrix[currentx + 1][currenty + 1].traversable == true) && (matrix[currentx + 1][currenty + 1].visited == false) && (currentx + 1 <= mapsize_x) && (currenty + 1 <= mapsize_y)) {
			if (calculate_h(endx, endy, currentx + 1, currenty + 1) < new_currenth) {
				new_currenth = calculate_h(endx, endy, currentx + 1, currenty + 1);
				newx = currentx + 1;
				newy = currenty + 1;
			}
		}
		if ((matrix[currentx - 1][currenty - 1].traversable == true) && (matrix[currentx - 1][currenty - 1].visited == false) && (currentx - 1 >= 0) && (currenty - 1 >= 0)) {
			if (calculate_h(endx, endy, currentx - 1, currenty - 1) < new_currenth) {
				new_currenth = calculate_h(endx, endy, currentx - 1, currenty - 1);
				newx = currentx - 1;
				newy = currenty - 1;
			}
		}
		if ((matrix[currentx - 1][currenty + 1].traversable == true) && (matrix[currentx - 1][currenty + 1].visited == false) && (currentx - 1 >= 0) && (currenty + 1 <= mapsize_y)) {
			if (calculate_h(endx, endy, currentx - 1, currenty + 1) < new_currenth) {
				new_currenth = calculate_h(endx, endy, currentx - 1, currenty + 1);
				newx = currentx - 1;
				newy = currenty + 1;
			}
		}
		if ((matrix[currentx + 1][currenty - 1].traversable == true) && (matrix[currentx + 1][currenty - 1].visited == false) && (currentx + 1 <= mapsize_x) && (currenty - 1 >= 0)) {
			if (calculate_h(endx, endy, currentx + 1, currenty - 1) < new_currenth) {
				new_currenth = calculate_h(endx, endy, currentx + 1, currenty - 1);
				newx = currentx + 1;
				newy = currenty - 1;
			}
		}
		*/
		if ((newx == -99) || (newy == -99)){
			currentx = mainStack.top().x;
			currenty = mainStack.top().y;
			mainStack.pop();
		}
		else {
			currentx = newx;
			currenty = newy;
			mainStack.push(matrix[currentx][currenty]);
			matrix[currentx][currenty].visited = true;
		}
		if ((currentx == endx) && (currenty == endy)) {
			found = true;
			stack<pathfind_matrix> da;

			int ss = mainStack.size();
			for (int n = 0; n < ss; n++) {
				da.push(mainStack.top());
				mainStack.pop();
			}
			return da;
		}
		if (mainStack.empty()) {

			found = true;
			stack<pathfind_matrix> da;
			return da;
		}
	}
}

SDL_Texture* getTexture(char* filename, SDL_Renderer *renderer) {
	SDL_Surface *surf = SDL_LoadBMP(filename);
	Uint32 colorkey = SDL_MapRGB(surf->format, 0xFF, 0, 0xFF);
	SDL_SetColorKey(surf, SDL_TRUE, colorkey);
	SDL_Texture *tex = SDL_CreateTextureFromSurface(renderer, surf);
	SDL_FreeSurface(surf);
	return tex;
}

vector<int> getClosestTree2(int xs, int ys, map world_map[mapsize_x][mapsize_y]) {
	vector<int> returning;
	bool found = false;
	int counter = 0;
	int counter2 = 1;
	int newx;
	int newy;
	int dir = 0; //0 = RIGHT, 1 = UP, 2 = LEFT, 3 = DOWN
	while (!found) {
		if (dir == 0) { //RIGHT
			int i;
			for (i = 0; i < counter2; i++) {
				if (world_map[xs + i][ys].objecttype == 1) {
					returning.push_back(xs + i);
					returning.push_back(ys);
					return returning;
				}
			}
			if (counter == 1) {
				dir = 1;
				counter++;
				xs = xs + i;
			}
			else {
				counter2++;
			}
		}
		else if (dir == 1) { //UP
			int i;
			for (i = 0; i < counter2; i++) {
				if (world_map[xs][ys + i].objecttype == 1) {
					returning.push_back(xs);
					returning.push_back(ys + i);
					return returning;
				}
			}
			if (counter == 1) {
				dir = 2;
				counter++;
				ys = ys + i;
			}
			else {
				counter2++;
			}
		}
		else if (dir == 2) { //LEFT
			int i;
			for (i = 0; i < counter2; i++) {
				int newxs = xs - 1;
				if (newxs < 0) {
					newxs = 0;
				}
				if (world_map[newxs][ys].objecttype == 1) {
					returning.push_back(newxs);
					returning.push_back(ys);
					return returning;
				}
			}
			if (counter == 1) {
				dir = 3;
				counter++;
				ys = ys - i;
			}
			else {
				counter2++;
			}
		}
		else if (dir == 3) { //DOWN
			int i;
			for (i = 0; i < counter2; i++) {
				int newys = ys - i;
				if (newys < 0) {
					newys = 0;
				}
				if (world_map[xs][newys].objecttype == 1) {
					returning.push_back(xs);
					returning.push_back(newys);
					return returning;
				}
			}
			if (counter == 1) {
				dir = 0;
				counter++;
				ys = ys - i;
			}
			else {
				counter2++;
			}
		}
	}
}

vector<int> getClosestTree(int xs, int ys, map world_map[mapsize_x][mapsize_y]) {
	// Check point (xs, ys)
	vector<int> returning;
	for (int d = 1; d<50; d++)
	{
		for (int i = 0; i < d + 1; i++)
		{
			int x1 = xs - d + i;
			if (x1 < 0) {
				x1 = 0;
			}
			int y1 = ys - i;
			if (y1 < 0) {
				y1 = 0;
			}

			// Check point (x1, y1)
			if (world_map[x1][y1].objecttype == 1) {
				returning.push_back(x1);
				returning.push_back(y1);
				return returning;
			}

			int x2 = xs + d - i;
			if (x2 < 0) {
				x2 = 0;
			}
			int y2 = ys + i;
			if (y2 < 0) {
				y2 = 0;
			}

			// Check point (x2, y2)
			if (world_map[x2][y2].objecttype == 1) {
				returning.push_back(x2);
				returning.push_back(y2);
				return returning;
			}
		}


		for (int i = 1; i < d; i++)
		{
			int x1 = xs - i;
			if (x1 < 0) {
				x1 = 0;
			}
			int y1 = ys + d - i;
			if (y1 < 0) {
				y1 = 0;
			}
			// Check point (x1, y1)
			if (world_map[x1][y1].objecttype == 1) {
				returning.push_back(x1);
				returning.push_back(y1);
				return returning;
			}

			int x2 = xs + i;
			if (x2 < 0) {
				x2 = 0;
			}
			int y2 = ys - d + i;
			if (y2 < 0) {
				y2 = 0;
			}

			// Check point (x2, y2)
			if (world_map[x2][y2].objecttype == 1) {
				returning.push_back(x2);
				returning.push_back(y2);
				return returning;
			}
		}
	}
}

void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, int sizex, int sizey) {
	//Setup the destination rectangle to be at the position we want
	SDL_Rect dst;
	dst.x = x;
	dst.y = y;
	dst.w = sizex;
	dst.h = sizey;

	//Query the texture to get its width and height to use
	//SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);
	SDL_RenderCopy(ren, tex, NULL, &dst);
}

vector<person> personlist;

int main(int argc, char *argv[]) {
	map map[mapsize_x][mapsize_y];
	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window *window = SDL_CreateWindow("GameTitle", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_HEIGHT, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);
	SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	srand(time(NULL));
	
	personlist.push_back(person{ 5, 5 , 5, 5 });
	personlist.push_back(person{ 2, 2, 2, 2 });

	SDL_Texture* texture_grass = getTexture("grass2.bmp", renderer);
	SDL_Texture* test = getTexture("test.bmp", renderer);
	SDL_Texture* texture_human = getTexture("human.bmp", renderer);
	SDL_Texture* texture_water = getTexture("water.bmp", renderer);
	SDL_Texture* texture_tree = getTexture("tree.bmp", renderer);
	SDL_RenderClear(renderer);

	//Creating Map
	for (int i = 0; i < mapsize_x; i++) {
		for (int j = 0; j < mapsize_y; j++) {
			uint32_t seed = 123545;
			const siv::PerlinNoise perlin(time(NULL));
			long float das = perlin.noise0_1(i + 0.5, j + 0.5);
			//printf("%f\n", das);
			if (das < 0.75) {
				map[i][j].tiletype = 0;
				map[i][j].objecttype = 0;
			}
			else {
				map[i][j].tiletype = 0;
				map[i][j].objecttype = 1;
			}
		}
	}
	// 2,2 and 5, 5 are pawn starting points
	map[2][2].tiletype = 0;
	map[2][2].objecttype = 0;
	map[5][5].tiletype = 0;
	map[5][5].objecttype = 0;

	//Displaying Map
	for (int i = 0; i < mapsize_x; i++) {
		for (int j = 0; j < mapsize_y; j++) {
			if (map[i][j].tiletype == 0) {
				renderTexture(texture_grass, renderer, i * tileSize, j * tileSize, tileSize, tileSize);
			}
			if (map[i][j].objecttype == 1) {
				renderTexture(texture_tree, renderer, i * tileSize + 3, j * tileSize + 3, 15, tileSize - 5);
			}
		}
	}

	//MAIN LOOP
	SDL_Event e;
	bool quit = false;
	//personlist[0].movelist = pathfind(2, 2, 31, 30, map);
	while (!quit) {
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) {
				quit = true;
			}
			if (e.type == SDL_KEYDOWN) {
				quit = true;
			}
			if (e.type == SDL_MOUSEBUTTONDOWN) {
				quit = true;
			}
		}
		
		//Displaying people
		SDL_RenderPresent(renderer);
		//Assign new movement if no movement
		for (int z = 0; z < personlist.size(); z++) {
			if (personlist[z].movelist.empty()) {
				personlist[z].prevx = personlist[z].x;
				personlist[z].prevy = personlist[z].y;
				int dir = 0; //0 middle 1 right, 2 up, 3 left, 4 down
				vector<int> r = getClosestTree(personlist[z].prevx, personlist[z].prevy, map);
				while (personlist[z].movelist.empty()) {
					if (dir == 0) {
						personlist[z].targetx = r[0];
						personlist[z].targety = r[1];
						personlist[z].movelist = pathfind(personlist[z].prevx, personlist[z].prevy, personlist[z].targetx + 1, personlist[z].targety, map);
						dir = 1;
					}
					else if (dir == 1) {
						personlist[z].targetx = r[0];
						personlist[z].targety = r[1];
						personlist[z].movelist = pathfind(personlist[z].prevx, personlist[z].prevy, personlist[z].targetx - 1, personlist[z].targety, map);
						dir = 2;
					}
					else if (dir == 2) {
						personlist[z].targetx = r[0];
						personlist[z].targety = r[1];
						personlist[z].movelist = pathfind(personlist[z].prevx, personlist[z].prevy, personlist[z].targetx, personlist[z].targety + 1, map);
						dir = 3;
					}
					else if (dir == 3) {
						personlist[z].targetx = r[0];
						personlist[z].targety = r[1];
						personlist[z].movelist = pathfind(personlist[z].prevx, personlist[z].prevy, personlist[z].targetx, personlist[z].targety - 1, map);
						dir = 0;
					}
					
					//SDL_Delay(5000);
					
				}
			}
		}
		//SDL_Delay(2000);
		//Get each actor's latest move
		for (int q = 0; q < personlist.size(); q++) {
			if (!personlist[q].movelist.empty()) {
				int recentx = personlist[q].movelist.top().x;
				int recenty = personlist[q].movelist.top().y;
				personlist[q].moveTo(recentx, recenty);
				personlist[q].movelist.pop();
				//remove tree
				if (personlist[q].movelist.empty()) {
					map[personlist[q].targetx][personlist[q].targety].objecttype = 0;
					renderTexture(texture_grass, renderer, personlist[q].targetx * tileSize, personlist[q].targety * tileSize, tileSize, tileSize);
				}
			}
		}
		//Display actors in their new positions
		for (int q = 0; q < personlist.size(); q++) {
			int previous_map_tiletype = map[personlist[q].getPrevX() / tileSize][personlist[q].getPrevY() / tileSize].tiletype;
			
			//if (previous_map_tiletype == 0) {
			//	renderTexture(texture_grass, renderer, personlist[q].getPrevX(), personlist[q].getPrevY(), tileSize, tileSize);
			//}
			renderTexture(texture_grass, renderer, personlist[q].getPrevX(), personlist[q].getPrevY(), tileSize, tileSize);
			renderTexture(texture_human, renderer, personlist[q].getX() + 7, personlist[q].getY() + 2, 6, 16);
			//renderTexture(test, renderer, personlist[q].getPrevX(), personlist[q].getPrevY(), tileSize, tileSize);
			SDL_RenderPresent(renderer);
			//SDL_Delay(50);
		}
		SDL_Delay(20);
	}
	return 0;
}